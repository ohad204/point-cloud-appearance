import torch
import argparse

from nerf.provider import NeRFDataset
from nerf.utils import *

from nerf.gui import NeRFGUI
import wandb
import datetime
# torch.autograd.set_detect_anomaly(True)

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--text', default="A hamburger", help="text prompt")
    parser.add_argument('--text_pc', default="a blue bathtub", help="PC text prompt")

    parser.add_argument('--expr_name', default="A hamburger", help="text prompt")
    parser.add_argument('--negative', default='', type=str, help="negative text prompt")
    parser.add_argument('-O', action='store_true', help="equals --fp16 --cuda_ray --dir_text")
    #parser.add_argument('-O2', action='store_true', help="equals --backbone vanilla --dir_text")
    parser.add_argument('-O2', type=bool, default=True, help="equals --backbone vanilla --dir_text")
    parser.add_argument('--test', action='store_true', help="test mode")
    parser.add_argument('--save_mesh', action='store_true', help="export an obj mesh with texture")
    parser.add_argument('--eval_interval', type=int, default=10, help="evaluate on the valid set every interval epochs")
    parser.add_argument('--workspace', type=str, default='workspace')
    parser.add_argument('--guidance', type=str, default='stable-diffusion', help='choose from [stable-diffusion, clip]')

    parser.add_argument('--point_cloud_file', type=str, default='point_cloud_bunny_90_nohead.obj', help='choose from [stable-diffusion, clip]')
    parser.add_argument('--scale', action='store_true', help='scaling the input PC')
    parser.add_argument('--gt_mesh_file', type=str, default='', help='choose from [stable-diffusion, clip]')
    parser.add_argument('--seed', type=int, default=0)

    #
    parser.add_argument('--config_id', type=int, default=0)

    ### training options
    parser.add_argument('--iters', type=int, default=10000, help="training iters")
    parser.add_argument('--lr', type=float, default=1e-3, help="max learning rate")
    parser.add_argument('--warm_iters', type=int, default=500, help="training iters")
    parser.add_argument('--min_lr', type=float, default=1e-4, help="minimal learning rate")
    parser.add_argument('--ckpt', type=str, default='latest')
    parser.add_argument('--cuda_ray', action='store_true', help="use CUDA raymarching instead of pytorch")
    parser.add_argument('--max_steps', type=int, default=512, help="max num steps sampled per ray (only valid when using --cuda_ray)")
    parser.add_argument('--num_steps', type=int, default=64, help="num steps sampled per ray (only valid when not using --cuda_ray)")
    parser.add_argument('--upsample_steps', type=int, default=32, help="num steps up-sampled per ray (only valid when not using --cuda_ray)")
    parser.add_argument('--update_extra_interval', type=int, default=16, help="iter interval to update extra status (only valid when using --cuda_ray)")
    parser.add_argument('--max_ray_batch', type=int, default=4096, help="batch size of rays at inference to avoid OOM (only valid when not using --cuda_ray)")
    parser.add_argument('--albedo', action='store_true', help="only use albedo shading to train, overrides --albedo_iters")
    parser.add_argument('--albedo_iters', type=int, default=1000, help="training iters that only use albedo shading")
    parser.add_argument('--uniform_sphere_rate', type=float, default=0.5, help="likelihood of sampling camera location uniformly on the sphere surface area")
    # model options
    parser.add_argument('--num_positional_enc', type=int, default=6, help="num of positional encoding of nerf")
    parser.add_argument('--radius_initialization', type=float, default=0.5, help="num of positional encoding of nerf")
    parser.add_argument('--bg_radius', type=float, default=0, help="if positive, use a background model at sphere(bg_radius)")
    parser.add_argument('--density_thresh', type=float, default=10, help="threshold for density grid to be occupied")
    parser.add_argument('--blob_density', type=float, default=10, help="max (center) density for the gaussian density blob")
    parser.add_argument('--blob_radius', type=float, default=0.3, help="control the radius for the gaussian density blob")
    # network backbone
    parser.add_argument('--fp16', action='store_true', help="use amp mixed precision training")
    parser.add_argument('--backbone', type=str, default='grid', choices=['grid', 'vanilla'], help="nerf backbone")
    parser.add_argument('--optim', type=str, default='adan', choices=['adan', 'adam', 'adamw'], help="optimizer")
    parser.add_argument('--sd_version', type=str, default='2.0', choices=['1.5', '2.0'], help="stable diffusion version")
    parser.add_argument('--hf_key', type=str, default=None, help="hugging face Stable diffusion model key")
    # rendering resolution in training, decrease this if CUDA OOM.

    
    parser.add_argument('--w', type=int, default=64, help="render width for NeRF in training")
    parser.add_argument('--h', type=int, default=64, help="render height for NeRF in training")
    parser.add_argument('--jitter_pose', action='store_true', help="add jitters to the randomly sampled camera poses")
    
    ### dataset options
    parser.add_argument('--bound', type=float, default=1, help="assume the scene is bounded in box(-bound, bound)")
    parser.add_argument('--dt_gamma', type=float, default=0, help="dt_gamma (>=0) for adaptive ray marching. set to 0 to disable, >0 to accelerate rendering (but usually with worse quality)")
    parser.add_argument('--min_near', type=float, default=0.1, help="minimum near distance for camera")
    parser.add_argument('--radius_range', type=float, nargs='*', default=[1.0, 1.5], help="training camera radius range")
    parser.add_argument('--fovy_range', type=float, nargs='*', default=[40, 70], help="training camera fovy range")
    parser.add_argument('--dir_text', action='store_true', help="direction-encode the text prompt, by appending front/side/back/overhead view")
    parser.add_argument('--suppress_face', action='store_true', help="also use negative dir text prompt.")
    parser.add_argument('--angle_overhead', type=float, default=30, help="[0, angle_overhead] is the overhead region")
    parser.add_argument('--angle_front', type=float, default=60, help="[0, angle_front] is the front region, [180, 180+angle_front] the back region, otherwise the side region.")

    parser.add_argument('--lambda_entropy', type=float, default=1e-4, help="loss scale for alpha entropy")
    parser.add_argument('--lambda_opacity', type=float, default=0, help="loss scale for alpha value")
    parser.add_argument('--lambda_orient', type=float, default=1e-2, help="loss scale for orientation")
    parser.add_argument('--lambda_smooth', type=float, default=0, help="loss scale for surface smoothness")

    parser.add_argument('--lambda_shape', type=float, default=1e6, help="loss for point cloud guidance")
    parser.add_argument('--lambda_eikonal_surface', type=float, default=1e4, help="eikonal loss on the pc surface")
    parser.add_argument('--lambda_eikonal_uniform', type=float, default=1e4, help="eikonal loss all over the volume")
    parser.add_argument('--lambda_sparsity', type=float, default=1e2, help="the volume should be sparse")

    ### GUI options
    parser.add_argument('--gui', action='store_true', help="start a GUI")
    # parser.add_argument('--use_wandb', action='store_true', help="start a GUI")
    parser.add_argument('--use_wandb', type=int, default=True, help="start a GUI")
    # parser.add_argument('--no_sds', action='store_true', help="start a GUI")
    parser.add_argument('--no_sds', type=int, default=False, help="start a GUI")
    parser.add_argument('--W', type=int, default=800, help="GUI width")
    parser.add_argument('--H', type=int, default=800, help="GUI height")
    parser.add_argument('--radius', type=float, default=3, help="default GUI camera radius from center")
    parser.add_argument('--fovy', type=float, default=60, help="default GUI camera fovy")
    parser.add_argument('--light_theta', type=float, default=60, help="default GUI light direction in [0, 180], corresponding to elevation [90, -90]")
    parser.add_argument('--light_phi', type=float, default=0, help="default GUI light direction in [0, 360), azimuth")
    parser.add_argument('--max_spp', type=int, default=1, help="GUI rendering max sample per pixel")

    opt = parser.parse_args()

    config_text = ['an airplane taking off from the runway', 'a duck swimming in a bath',
                   'a little kid jumping on a bed',
                   'a kangaroo sitting on a bench', 'a wood bookshelf with 3 toys',
                   'a humanoid robot using a rolling pin to roll out dough',
                   'a man wearing virtual reality headset', 'a brown cup with whipped cream and a straw', 'a stand lamp with toys on the floor',
                   'a cone of ice cream and candies',
                   'Carnivorous plant in a brown vase',
                   'a red light bulb on the ceiling',
                   'a man with a mexican hat',
                   'a cat standing on a metal stand',
                   'a pink bottle of champagne wine',
                   'oranges inside a bowl',
                   'a computer on the table',
                   'an airplane with 4 engines under each of his wings', 'a frog having a bath',
                   'a tiger sleeping on the bed',
                   'a panda bear sitting on a metal bench', 'a wood bookshelf with colorful books',
                   'a person holding a sword',
                   'a man with a fox head', 'a brown cup with whipped cream', 'a stand lamp near a sofa',
                   'a cone of ice cream',
                   'flowers bouquet in a pink vase',
                   'a light bulb',
                   'a man with a hat',
                   'a LCD tv on a wood stand',
                   'a green bottle with a note',
                   'bananas inside a bowl',
                   'a kid hiding under the table']
    config_text_pc = ['an airplane', 'a vlue bath',
                   'a bed',
                   'a wood bench', 'a wood bookshelf',
                   'a humanoid robot',
                   'a man', 'a brown cup', 'a stand lamp',
                   'a cone',
                   'a brown vase',
                   'a red light bulb',
                   'a mexican man',
                   'a metal stand',
                   'a pink bottle',
                   'a wood bowl',
                   'a table',
                   'an airplane', 'a yellow bath',
                   'a wood bed',
                   'a metal bench', 'a wood bookshelf',
                   'a person',
                   'a man', 'a brown cup', 'a stand lamp',
                   'a cone',
                   'a pink vase',
                   'a bulb',
                   'a man',
                   'a wood stand',
                   'a green bottle',
                   'a bowl',
                   'a table']
    config_point_cloud_file = ['shapes/airplane_0628.obj', 'shapes/bathtub_0107.obj', 'shapes/bed_0528.obj',
                               'shapes/bench_0179.obj', 'shapes/bookshelf_0580.obj', 'shapes/person_0075.obj',
                               'shapes/person_0075_headless.obj', 'shapes/cup_0082.obj', 'shapes/lamp_0134.obj',
                               'shapes/points_cone_0179.obj',
                               'shapes/vase_0501.obj',
                               'shapes/flower_pot_0153_new.obj',
                               'shapes/person_0075_headless.obj',
                               'shapes/glass_box_0190.obj',
                               'shapes/bottle_0024.obj',
                               'shapes/bowl_0027.obj',
                               'shapes/table_0026.obj',
                               'shapes/airplane_0628.obj', 'shapes/bathtub_0107.obj', 'shapes/bed_0528.obj',
                               'shapes/bench_0179.obj', 'shapes/bookshelf_0580.obj', 'shapes/person_0075.obj',
                               'shapes/person_0075_headless.obj', 'shapes/cup_0082.obj', 'shapes/lamp_0134.obj',
                               'shapes/points_cone_0179.obj',
                               'shapes/vase_0501.obj',
                               'shapes/flower_pot_0153_new.obj',
                               'shapes/person_0075_headless.obj',
                               'shapes/glass_box_0190.obj',
                               'shapes/bottle_0024.obj',
                               'shapes/bowl_0027.obj',
                               'shapes/table_0026.obj']

    opt.workspace = 'experiments/' + str(datetime.datetime.now())

    opt.text = config_text[opt.config_id]
    opt.text_pc = config_text_pc[opt.config_id]
    opt.point_cloud_file = config_point_cloud_file[opt.config_id]

    use_wandb=opt.use_wandb
    if use_wandb:
        experiment_string=opt.text + ' - shape:' + str(opt.lambda_shape) + ' - eik_surface:' + str(opt.lambda_eikonal_surface) + ' - eik_uniform:' + str(opt.lambda_eikonal_uniform) + ' - pos_enc:' + str(opt.num_positional_enc) + ' - radius:' + str(opt.radius_initialization)
        wandb.login(key="c6e8af2a104f2bfe4149fe9f42dd213ba9652b4e")
        #wandb.init(project='deformable_surface_people_pifu',entity='nvr-israel',name="transformer_depth_%d__heads_%d__mlp_dim_%d__detach_%d__pretrain=%d__sep_E_%d_PT_%d_Ptch_%d"%(opt.transformer_depth,opt.transformer_heads,opt.transformer_mlp_dim,opt.transformer_detach_features_gradient,opt.load_netG_checkpoint_path is not None,opt.use_special_filter_for_transformer,opt.use_special_filter_for_transformer_pretrained,opt.transformer_patch_size),config=vars(opt))
        wandb.init(project='pc_completion_txt',name=experiment_string,config=vars(opt))#transformer_depth_%d__heads_%d__mlp_dim_%d__detach_%d"%(opt.transformer_depth,opt.transformer_heads,opt.transformer_mlp_dim,opt.transformer_detach_features_gradient),config=vars(opt))
       

    if opt.O:
        opt.fp16 = True
        opt.dir_text = True
        opt.cuda_ray = True

    elif opt.O2:
        # only use fp16 if not evaluating normals (else lead to NaNs in training...)
        if opt.albedo:
            opt.fp16 = True
        opt.dir_text = True
        opt.backbone = 'vanilla'

    if opt.albedo:
        opt.albedo_iters = opt.iters

    if opt.backbone == 'vanilla':
        from nerf.network import NeRFNetwork
    elif opt.backbone == 'grid':
        from nerf.network_grid import NeRFNetwork
    else:
        raise NotImplementedError(f'--backbone {opt.backbone} is not implemented!')

    print(opt)

    seed_everything(opt.seed)

    model = NeRFNetwork(opt)

    print(model)

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    if opt.test:
        guidance = None # no need to load guidance model at test

        trainer = Trainer('df', opt, model, guidance,opt.point_cloud_file,opt.gt_mesh_file, device=device, workspace=opt.workspace, fp16=opt.fp16, use_checkpoint=opt.ckpt)

        if opt.gui:
            gui = NeRFGUI(opt, trainer)
            gui.render()
        
        else:
            test_loader = NeRFDataset(opt, device=device, type='test', H=opt.H, W=opt.W, size=100).dataloader()
            # trainer.test(test_loader)
            
            if opt.save_mesh:
                trainer.save_mesh(resolution=256)
    
    else:
        
        train_loader = NeRFDataset(opt, device=device, type='train', H=opt.h, W=opt.w, size=100).dataloader()

        if opt.optim == 'adan':
            from optimizer import Adan
            # Adan usually requires a larger LR
            optimizer = lambda model: Adan(model.get_params(5 * opt.lr), eps=1e-8, weight_decay=2e-5, max_grad_norm=5.0, foreach=False)
        else: # adam
            optimizer = lambda model: torch.optim.Adam(model.get_params(opt.lr), betas=(0.9, 0.99), eps=1e-15)

        if opt.backbone == 'vanilla':
            warm_up_with_cosine_lr = lambda iter: iter / opt.warm_iters if iter <= opt.warm_iters \
                else max(0.5 * ( math.cos((iter - opt.warm_iters) /(opt.iters - opt.warm_iters) * math.pi) + 1), 
                         opt.min_lr / opt.lr)

            scheduler = lambda optimizer: optim.lr_scheduler.LambdaLR(optimizer, warm_up_with_cosine_lr)
        else:
            scheduler = lambda optimizer: optim.lr_scheduler.LambdaLR(optimizer, lambda iter: 1) # fixed
            # scheduler = lambda optimizer: optim.lr_scheduler.LambdaLR(optimizer, lambda iter: 0.1 ** min(iter / opt.iters, 1))

        if opt.guidance == 'stable-diffusion':
            from nerf.sd import StableDiffusion
            guidance = StableDiffusion(device, opt.sd_version, opt.hf_key)
        elif opt.guidance == 'clip':
            from nerf.clip import CLIP
            guidance = CLIP(device)
        else:
            raise NotImplementedError(f'--guidance {opt.guidance} is not implemented.')

        trainer = Trainer('df', opt, model, guidance,opt.point_cloud_file,opt.gt_mesh_file, device=device, workspace=opt.workspace, optimizer=optimizer, ema_decay=None, fp16=opt.fp16, lr_scheduler=scheduler, use_checkpoint=opt.ckpt, eval_interval=opt.eval_interval, scheduler_update_every_step=True)

        if opt.gui:
            trainer.train_loader = train_loader # attach dataloader to trainer

            gui = NeRFGUI(opt, trainer)
            gui.render()
        
        else:
            valid_loader = NeRFDataset(opt, device=device, type='val', H=opt.H, W=opt.W, size=5).dataloader()

            max_epoch = np.ceil(opt.iters / len(train_loader)).astype(np.int32)
            trainer.train(train_loader, valid_loader, max_epoch)