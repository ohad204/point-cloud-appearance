

import trimesh
from pathlib import Path
import numpy as np

mesh_file="mesh_files/mesh_bunny_normalized.obj"

Path("%s_samples"%mesh_file).mkdir(parents=True, exist_ok=True)

sample_points=[100,500,1000,2000,5000,10000]


mesh=trimesh.load_mesh(mesh_file)

for samples in sample_points:
    aaa=np.array(trimesh.sample.sample_surface(mesh,samples)[0])
    trimesh.PointCloud(aaa).export("%s_samples/%d.obj"%(mesh_file,samples))